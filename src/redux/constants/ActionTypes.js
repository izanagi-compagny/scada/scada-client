export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';

export const API_START = 'API_START';
export const API_END = 'API_END';
export const API_ERROR = 'API_ERROR';
export const ACCESS_DENIED = 'ACCESS_DENIED';
export const NO_UPDATE_STATE = 'NO_UPDATE_STATE';
export const API = 'API';

export const FETCH_REPORTS_USER = 'FETCH_REPORTS_USER';
export const FIND_REPORTS_USER_BY_EMAIL = 'FIND_REPORTS_USER_BY_EMAIL';
export const SET_REPORTS_USER = 'SET_REPORTS_USER';

export const FETCH_USER_REPORT = 'FETCH_USER_REPORT';
export const SET_USER_REPORT = 'SET_USER_REPORT';


export const UPDATE_USER_REPORT = 'UPDATE_USER_REPORT';
