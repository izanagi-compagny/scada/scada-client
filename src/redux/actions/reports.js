import {apiAction, apiEnd, noUpdateState} from "./api";
import {
    FETCH_REPORTS_USER,
    FETCH_USER_REPORT, FIND_REPORTS_USER_BY_EMAIL,
    SET_REPORTS_USER,
    SET_USER_REPORT,
    UPDATE_USER_REPORT
} from "../constants/ActionTypes";

export const fetchUsers = () => apiAction({
        url: "http://localhost:8080/reports/users",
        onSuccess: setUsers,
        onFailure: () => { console.log('Error while fetching user')},
        label: FETCH_REPORTS_USER
    });

export const findUserByEmail = (email) => apiAction({
        url: `http://localhost:8080/reports/users/${email}`,
        onSuccess: setUsers,
        onFailure: () => { console.log('Error while fetching user')},
        label: FIND_REPORTS_USER_BY_EMAIL
    });

export const fetchUserReport = id => apiAction({
        url: `http://localhost:8080/reports/${id}`,
        onSuccess: setUserReport,
        onFailure: () => { console.log('Error while fetching user')},
        label: FETCH_USER_REPORT
    })

export const updateReportStatus = (userId, reportId, statusId) => apiAction({
    url: `http://localhost:8080/reports/`,
    method: 'PUT',
    onSuccess: (data, dispatch) => { dispatch(fetchUserReport(userId)); return noUpdateState(UPDATE_USER_REPORT)},
    onFailure: () => { console.log('Error while fetching user')},
    data: { reportId: reportId, statusId: statusId },
    label: UPDATE_USER_REPORT
})

const setUserReport = (data) => {
    return {
        type:SET_USER_REPORT,
        payload: data
    }
}

const setUsers = (data) => {
    return {
        type: SET_REPORTS_USER,
        payload: data
    }
}
