import {SET_REPORTS_USER, SET_USER_REPORT} from '../constants/ActionTypes';

const initialState = {
    users: [],
    reports: []
};

export default function report(state = initialState, action) {
    switch (action.type) {
        case SET_REPORTS_USER:
            return {...state, users: action.payload.data}
        case SET_USER_REPORT:
            return {...state, reports: action.payload.data}
        default:
            return state;
    }
}