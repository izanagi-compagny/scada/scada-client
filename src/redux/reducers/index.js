import { combineReducers } from 'redux';

import report from "./reports";


const reducers = combineReducers({
    reports: report
});

export default reducers;
