import React from 'react';
import { Switch, Route } from "react-router-dom";
import ReportInterface from "./components/ReportInterface";

import ReportList from "./components/ReportList";
function App() {
  return (
    <div>

        <Switch>
            <Route exact path="/reports/:userId" render={ (props) => <ReportList {...props} /> } />
            <Route path="/">
                <ReportInterface />
            </Route>
        </Switch>
    </div>

  );
}

export default App;
