import React from "react";
import { connect } from "react-redux";
import {fetchUserReport, fetchUsers, updateReportStatus} from "../redux/actions/reports";

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

class ReportList extends React.Component {

    constructor(props) {
        super(props)
        this.updateReportStatus = this.updateReportStatus.bind(this);
        this.userId = this.props.match.params.userId;
    }

    componentDidMount() {
        this.props.fetchReport(this.userId);
    }

    updateReportStatus(userId, reportId) {

        this.props.updateReport(userId, reportId.toString(), "3");
    };

    render() {

        return (
            <div>
                <TableContainer component={Paper}>
                    <Table  aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell >id</TableCell>
                                <TableCell align="center">type_id</TableCell>
                                <TableCell align="center">status_id</TableCell>
                                <TableCell align="center">user_id</TableCell>
                                <TableCell align="center">report_id</TableCell>
                                <TableCell align="center">FIX</TableCell>

                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.props.reports.map((row) => (
                                <TableRow key={row.id} >
                                    <TableCell component="th" scope="row">
                                        {row.id}
                                    </TableCell>
                                    <TableCell align="center">{row.type_id}</TableCell>
                                    <TableCell align="center">{row.status_id}</TableCell>
                                    <TableCell align="center">{row.user_id}</TableCell>
                                    <TableCell align="center">{row.report_id}</TableCell>
                                    <TableCell align="center"><button onClick={ () => this.updateReportStatus(this.userId, row.report_id)}>Fix</button></TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        );
    }
}



const mapStateToProps = state => {
    return { reports: state.reports.reports };
};

function mapDispatchToProps(dispatch) {
    return {
        fetchReport: (id) => dispatch(fetchUserReport(id)),
        updateReport: (userId, reportId, statusId) => dispatch(updateReportStatus(userId, reportId, statusId))
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(ReportList);
