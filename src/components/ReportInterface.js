import React from "react";
import { connect } from "react-redux";
import {fetchUsers, findUserByEmail} from "../redux/actions/reports";
import { Link } from "react-router-dom";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TextField from "@material-ui/core/TextField";
import {TableReport} from "./TableReport";

class ReportInterface extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            search: ''
        }

        this.handleInsert = this.handleInsert.bind(this);
    }

    componentDidMount() {
        this.props.fetchUser();
    }

    handleInsert(event) {
        event.preventDefault();
        const email = event.target.value;
        this.setState({search: email})

        this.props.findUserByEmail(email)
    }

    render() {
        return (
            <div>
                <TextField id="standard-basic" value={this.state.search} onChange={this.handleInsert} label="Email" />
                {this.props.users.length === 0 ? <p> no users found</p> : <TableReport users={this.props.users} />}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return { users: state.reports.users };
};

function mapDispatchToProps(dispatch) {
    return {
        fetchUser: () => dispatch(fetchUsers()),
        findUserByEmail: (email) => dispatch(findUserByEmail(email))
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(ReportInterface);
